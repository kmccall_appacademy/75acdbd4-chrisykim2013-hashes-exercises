# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  lnth = {}
  str.split.each do |word|
    lnth[word] = word.length
  end
  lnth
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k, v| v}[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  count = Hash.new(0)
  word.each_char {|ch| count[ch] += 1}
  count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  new_hash = {}
  arr.each do |el|
    new_hash[el] = 0
  end
  new_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  new_hash = Hash.new(0)
  numbers.each do |number|
    if number.even?
      new_hash[:even] += 1
    else
      new_hash[:odd] += 1
    end
  end
  new_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel = "aeiou"
  new_hash = Hash.new(0)
  string.each_char {|ch| new_hash[ch] += 1 if vowel.include?(ch)}
  max_value = new_hash.values.max
  new_hash.select {|k, v| v == max_value}.sort.first.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half_students = students.select {|k, v| v > 6}.keys
  second_half_students.combination(2).to_a
end
#
# Second Solution Below:
# def fall_and_winter_birthdays(students)
#   second_half_students = students.select {|k, v| v > 6}.keys
#   res = []
#   second_half_students.each.with_index do |student1, idx1|
#     second_half_students.each.with_index do |student2, idx2|
#       next if idx1 == idx2
#       res << [student1, student2] unless res.include?([student2, student1])
#     end
#   end
#   res
# end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  num_of_species = specimens.uniq.count
  count_hash = Hash.new(0)
  specimens.each do |animal|
    count_hash[animal] += 1
  end
  sorted_arr = count_hash.values.sort
  num_of_species **2 * sorted_arr.min / sorted_arr.max
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count_hash = character_count(normal_sign)
  vandal_count_hash = character_count(vandalized_sign)
  vandal_count_hash.all? {|k, v| vandal_count_hash[k] <= normal_count_hash[k]}
end

def character_count(str)
  str.delete!(" ?!.,:;'")
  ch_count_hash = Hash.new(0)
  str.downcase.each_char {|ch| ch_count_hash[ch] += 1}
  ch_count_hash
end
